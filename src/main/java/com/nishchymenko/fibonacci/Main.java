package com.nishchymenko.fibonacci;

import com.nishchymenko.fibonacci.exception.RangeException;
import com.nishchymenko.fibonacci.logger.Logger;
import com.nishchymenko.fibonacci.model.Fibonacci;
import com.nishchymenko.fibonacci.model.Range;
import com.nishchymenko.fibonacci.validator.RangeValidator;
import java.util.Scanner;

public class Main {

  public static void main(final String[] args) {
    Logger.info("App starting...");

    while (true) {
      Range range = getRange();
      int setSize = getNumber();
      Fibonacci fibonacci = new Fibonacci(range, setSize);
      fibonacci.proceed();
    }

  }

  private static int getNumber() {
    System.out.print("Input fibonacci numbers count: ");
    int numb;

    while (true) {
      try {
        String input = consoleInput();

        Logger.info("User try to input number: " + input);

        numb = Integer.parseInt(input);
        break;
      } catch (RuntimeException e) {
        Logger.error("Dummy user: " + e);
        System.out.println("Input correct number: ");
      }
    }

    return numb;
  }

  private static String consoleInput() {
    Scanner scanner = new Scanner(System.in);
    String input = scanner.nextLine();

    if (input.equals("q") || input.equals("Q")) {
      System.exit(0);
    } else if (input.equals("lol")) {
      Logger.fatal("Big LOL");
      System.out.println("Don't do this again, man");
    }

    return input;
  }

  private static Range getRange() {
    System.out.print("Input range: ");
    Range range;
    RangeValidator rangeValidator = new RangeValidator();

    while (true) {
      try {
        String input = consoleInput();

        Logger.info("User try to input range: " + input);

        range = rangeValidator.parseRange(input);
        break;
      } catch (RangeException e) {
        Logger.error("Dummy user: " + e);
        System.out.println(e.getMessage());
      }
    }
    return range;
  }
}
