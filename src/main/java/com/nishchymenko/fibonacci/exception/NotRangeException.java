package com.nishchymenko.fibonacci.exception;

public class NotRangeException extends RangeException {

  private static final String MESSAGE = "Inputted data is not a range";

  @Override
  public String getMessage() {
    return MESSAGE;
  }
}
