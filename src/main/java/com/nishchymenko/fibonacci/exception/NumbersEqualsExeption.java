package com.nishchymenko.fibonacci.exception;

public class NumbersEqualsExeption extends RangeException {

  private static final String MESSAGE = "Numbers should not be equals";
  @Override
  public String getMessage() {
    return MESSAGE;
  }
}
