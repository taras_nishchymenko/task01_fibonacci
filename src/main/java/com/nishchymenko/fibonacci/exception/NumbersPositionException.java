package com.nishchymenko.fibonacci.exception;

public class NumbersPositionException extends RangeException {

  private static final String MESSAGE = "Range should be growing";
  @Override
  public String getMessage() {
    return MESSAGE;
  }
}
