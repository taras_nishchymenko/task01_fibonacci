package com.nishchymenko.fibonacci.logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class Logger {

  private static String filePath =
      "/home/taras/IdeaProjects/Fibonacci/src/main/resources/fibonacci.log";

  public static void error(final String message) {
    write("ERROR: " + message + "\n");
  }

  public static void fatal(final String message) {
    write("FATAL: " + message + "\n");
  }

  public static void warning(final String message) {
    write("WARNING: " + message + "\n");
  }

  public static void info(final String message) {
    write("INFO: " + message + "\n");
  }

  private static void write(final String message) {
    File file = new File(filePath);
    try (FileWriter fileWriter = new FileWriter(file, true)) {
      Writer out = new BufferedWriter(fileWriter);
      out.append(message);
      out.flush();
    } catch (IOException e) {
      System.out.println("Something very bad happen... [tears]");
    }
  }
}
