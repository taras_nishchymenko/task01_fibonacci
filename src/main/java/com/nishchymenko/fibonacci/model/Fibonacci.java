package com.nishchymenko.fibonacci.model;

import com.nishchymenko.fibonacci.logger.Logger;

public class Fibonacci {

  private Range range;
  private int setSize;
  private int oddCount = 0;
  private static final double MAX_PERCENT = 100d;

  public Fibonacci(final Range range, final int setSize) {
    Logger.info("Fibonacci object created");

    this.range = range;
    this.setSize = setSize;
  }

  public void proceed() {
    Logger.info("Fibonacci is processing...");

    range.printOddFromBeginWithSum();
    range.printEvenFromEndWithSum();
    printFibonacciNumbersAndCountOddNumbers();
    printPercentages();
  }

  private void printPercentages() {
    double oddPercent = (MAX_PERCENT * oddCount) / setSize;
    System.out.println("Odd percent = " + oddPercent);
    System.out.println("Even percent = " + (MAX_PERCENT - oddPercent));
  }

  private void printFibonacciNumbersAndCountOddNumbers() {
    int first = range.getEnd() - 1;
    int second = range.getEnd();

    System.out.println("Fibonacci numbers: ");
    System.out.print(first + " ");
    increaseIfOdd(first);
    if (setSize == 1) {
      return;
    }
    System.out.print(second + " ");
    increaseIfOdd(second);
    if (setSize == 2) {
      return;
    }

    int counter = 3;

    while (counter <= setSize) {
      int sum = first + second;
      System.out.print(sum + " ");
      first = second;
      second = sum;

      increaseIfOdd(sum);

      counter++;
    }

    System.out.println();
  }

  private void increaseIfOdd(final int number) {
    if (number % 2 != 0) {
      oddCount++;
    }
  }
}
