package com.nishchymenko.fibonacci.model;

import com.nishchymenko.fibonacci.logger.Logger;

public class Range {

  private int begin;
  private int end;

  public Range(final int begin, final int end) {
    this.begin = begin;
    this.end = end;

    Logger.info("Range is created: " + this);
  }

  public void printOddFromBeginWithSum() {
    int sum = 0;

    System.out.println("Odd numbers from begin: ");

    for (int i = begin; i <= end; i++) {
      if (i % 2 != 0) {
        System.out.print(i + " ");
        sum += i;
      }
    }

    System.out.println();
    System.out.print("Odd numbers sum: ");
    System.out.println(sum);
  }

  public void printEvenFromEndWithSum() {
    int sum = 0;

    System.out.println("Even numbers from end: ");
    for (int i = end; i >= begin; i--) {
      if (i % 2 == 0) {
        System.out.print(i + " ");
        sum += i;
      }
    }
    System.out.println();
    System.out.print("Even numbers sum: ");
    System.out.println(sum);
  }

  public int getBegin() {
    return begin;
  }

  public int getEnd() {
    return end;
  }

  @Override
  public String toString() {
    return "Range{"
        + "begin=" + begin
        + ", end=" + end
        + '}';
  }
}
