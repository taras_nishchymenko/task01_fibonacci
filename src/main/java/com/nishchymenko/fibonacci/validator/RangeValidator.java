package com.nishchymenko.fibonacci.validator;

import com.nishchymenko.fibonacci.exception.NotRangeException;
import com.nishchymenko.fibonacci.exception.NumbersEqualsExeption;
import com.nishchymenko.fibonacci.exception.NumbersPositionException;
import com.nishchymenko.fibonacci.exception.RangeException;
import com.nishchymenko.fibonacci.logger.Logger;
import com.nishchymenko.fibonacci.model.Range;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RangeValidator {

  private static final String RANGE_REGEX =
      "^((\\[-?\\d+|-?\\d+|\\(-?\\d+)(, |,| |;|; )(-?\\d+|-?\\d+\\)))";
  private static final String NUMBERS_REGEX =
      "^\\D*(-?\\d+)((,+ *)|(,* +)|(;* +)|(;+ *))(-?\\d+)\\D*";
  private static final int GROUP_FOR_FIRST_NUMBER = 1;
  private static final int GROUP_FOR_SECOND_NUMBER = 7;

  public boolean isValidInput(final String inputLine) {

    Pattern pattern = Pattern.compile(RANGE_REGEX);
    Matcher matcher = pattern.matcher(inputLine);

    return matcher.matches();
  }

  public Range parseRange(final String inputLine) throws RangeException {
    Logger.info("Range is parsing: " + inputLine);

    if (!isValidInput(inputLine)) {
      Logger.warning("Now i'll throw an exception -_-");
      throw new NotRangeException();
    }

    Pattern pattern = Pattern.compile(NUMBERS_REGEX);
    Matcher matcher = pattern.matcher(inputLine);

    if (matcher.find()) {

      int begin = Integer.valueOf(matcher.group(GROUP_FOR_FIRST_NUMBER));
      int end = Integer.valueOf(matcher.group(GROUP_FOR_SECOND_NUMBER));

      if (begin > end) {
        Logger.warning("Now i'll throw an exception -_-");
        throw new NumbersPositionException();
      } else if (begin == end) {
        Logger.warning("Now i'll throw an exception -_-");
        throw new NumbersEqualsExeption();
      }

      return new Range(begin, end);
    }

    return null;
  }
}
